@extends('layouts.master')

@section('sub-title')
    Jawaban
@endsection

@section('content')

<a href="/jawaban/create" class="btn btn-primary btn-sm">Jawaban Baru</a>

<div class="row">
    @forelse($jawaban as $item)
    <div class="col-3">
        <div class="card">
            <img src="{{asset('/gambar/' . $item->gambar)}}" height="200px" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">{{$item->jawaban}}</h5>
              <a href="/jawaban{{$item->id}}" class="btn btn-info btn-block">Read More</a>
              <div class="row mt-3">
                <div class="col-6">
                    <a href="/jawaban/{{$item->id}}/edit" class="btn btn-warning btn-block">Edit</a>
                </div>
                <div class="col-6">
                    <form action="/jawaban/{{$item->id}}" method="post">
                      @csrf
                      @method('delete')
                      <input type="submit" class="btn btn-danger btn=block" value="delete">
                    </form>
                </div>
              </div>
            </div>
          </div>
    </div>
    @empty
        <h1>Tidak ada Jawaban</h1>
    @endforelse
</div>

@endsection
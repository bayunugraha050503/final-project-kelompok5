@extends('layouts.master')

@section('sub-title')
    Jawaban Baru
@endsection

@section('content')

<form action="/jawaban" method="post" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label>Jawaban</label>
        <input type='text' name="jawaban" class="form-control">
    </div>
    @error('jawaban')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Pertanyaan</label>
        <select name="pertanyaan_id" class="form-control" id="">
            <option value="">--Pilih Pertanyaan--</option>
            @forelse ($pertanyaan as $item)
                <option value="{{$item->id}}">{{$pertanyaan->pertanyaan}}</option>
            @empty
                <option value="">Belum ada Pertanyaan</option>
            @endforelse
        </select>
    </div>  
    @error('pertanyaan_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>User</label>
        <select name="user_id" class="form-control" id="">
            <option value="">--Pilih User--</option>
            @forelse ($user as $item)
                <option value="{{$item->id}}">{{$user->name}}</option>
            @empty
                <option value="">Belum ada user</option>
            @endforelse
        </select>
    </div>  
    @error('user_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Gambar</label>
        <input type='file' name="gambar" class="form-control">
    </div>  
    @error('gambar')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection
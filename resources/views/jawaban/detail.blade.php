@extends('layouts.master')

@section('sub-title')
    Detail Jawaban
@endsection

@section('content')

<div class="card">
    <img src="{{asset('/gambar/' . $jawaban->gambar)}}" class="card-img-top" alt="...">
    <div class="card-body">
      <h5 class="card-title">{{$jawaban->jawaban}}</h5>
    </div>
  </div>

  <a href="/jawaban" class="btn btn-secondary btn-sm">Kembali</a> 
  
@endsection
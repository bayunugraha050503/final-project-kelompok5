@extends('layouts.master')

@section('sub-title')
    Edit Jawaban
@endsection

@section('content')

<form action="/jawaban/{{$jawaban->id}}" method="post" enctype="multipart/form-data">
    @csrf
    @method('put')
    <div class="form-group">
        <label>Jawaban</label>
        <input type='text' name="jawaban" value="{{$jawaban->jawaban}}" class="form-control">
    </div>
    @error('jawaban')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Pertanyaan</label>
        <select name="pertanyaan_id" class="form-control" id="">
            <option value="">--Pilih Pertanyaan--</option>
            @forelse ($pertanyaan as $item)
                @if ($item->id === $pertanyaan->$pertanyaan_id)
                    <option value="{{$item->id}}" selected>{{$pertanyaan->pertanyaan]}}</option>
                @else
                    <option value="{{$item->id}}">{{$pertanyaan->pertanyaan}}"></option>
                @endif
            @empty
                <option value="">Belum ada Pertanyaan</option>
            @endforelse
        </select>
    </div>  
    @error('pertanyaan_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>User</label>
        <select name="user_id" class="form-control" id="">
            <option value="">--Pilih User--</option>
            @forelse ($user as $item)
                @if ($item->id === $pertanyaan->$user_id)
                    <option value="{{$item->id}}" selected>{{$user->name}}</option>
                @else
                    <option value="{{$item->id}}">{{$user->name}}</option>
                @endif
            @empty
                <option value="">Belum ada user</option>
            @endforelse
        </select>
    </div>  
    @error('user_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Gamber</label>
        <input type='file' name="gambar" class="form-control">
    </div>  
    @error('gambar')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection
@extends('layouts.master')

@section('title')
    Edit Kategori
@endsection

@section('content')

<form action="/kategori/{{$kategori->id}}" method="post">
    @csrf
    @method('put')
    <div class="form-group">
        <label>Nama Kategori</label>
        <input type='string' name="kategori" value="{{$kategori->kategori}}" class="form-control">
    </div>
    @error('kategori')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    @method('put')
    <div class="form-group">
        <label>Deskripsi Kategori</label>
        <input type='text' name="deskripsi" value="{{$kategori->deskripsi}}" class="form-control">
    </div>
    @error('deskripsi')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection
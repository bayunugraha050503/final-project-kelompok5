@extends('layouts.master')

@section('title')
    Detail Kategori
@endsection

@section('content')

<h1 class="text-primary">{{$kategori->kategori}}</h1>
<h3 class="text-body">Deskripsi:   </h3>
<p class="text-dark w-75 ">{{$kategori->deskripsi}}</p>
<a href="/kategori" class="btn btn-warning btn-sm">Back</a>

@endsection
@extends('layouts.master')

@section('title')
    Kategori 
@endsection

@section('content')


<a href="/kategori/create" class="btn btn-primary btn-sm">Kategori Baru</a>
<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Kategori</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($kategori as $key => $item)
    <tr>
        <th scope="row">{{$key+1}}</th>
        <td>{{$item->kategori}}</td>
        <td>
            <form action="/kategori/{{$item->id}}" method="post">
            <a href="/kategori/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
            <a href="/kategori/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                @csrf
                @method('delete')
                <input type="submit" value="delete" class="btn btn-danger btn-sm">
            </form>
        </td>
    </tr>
    @empty
        <h1>Data Kosong</h1> 
    @endforelse
  </tbody>
</table>

@endsection
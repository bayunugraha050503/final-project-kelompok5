@extends('layouts.master')

@section('title')
    Kategori Baru
@endsection

@section('content')

<form action="/kategori" method="post">
    @csrf
    <div class="form-group">
        <label>Nama Kategori</label>
        <input type='string' name="kategori" class="form-control">
    </div>
    @error('kategori')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Deksripsi</label>
        <input type='text' name="deskripsi" class="form-control">
    </div>  
    @error('deskripsi')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection
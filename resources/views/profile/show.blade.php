@extends('layouts.master')

@section('title')
    Halaman Profile
@endsection

@section('content')
    <a href="/profile/edit" class="btn btn-primary">Edit Profile</a><br><br>
    <div class="row">
        <div class="col-3">
            <img class="nav-profile-img" src="{{asset('/images/'.$profile->gambar)}}" onerror="this.src='{{asset('/images/alt.png')}}'" alt="" width="200px" height="200px"/>
        </div>

        <div class="col-9">
            <ul class="list-unstyled">
                <li>
                    <b>Nama:</b><br>
                    {{$profile->user->name}}
                </li>
                <li>
                    <b>Umur:</b><br>
                    {{$profile->age}} tahun
                </li>
                <li>
                    <b>Alamat:</b><br>
                    {{$profile->address}}
                </li>
                <li>
                    <b>Biodata:</b><br>
                    {{$profile->biodata}}
                </li>
            </ul>
            
        </div>
    </div>
    
@endsection
@extends('layouts.master')

@section('title')
    Edit Profile
@endsection

@section('content')
    <form action="/profile/{{$profile->id}}" method="post" enctype="multipart/form-data">
    @csrf
    @method('put')
    <div class="form-group">
        <label>Name</label>
        <input type="text" name="name" value="{{$profile->user->name}}" class="form-control" placeholder="Masukkan nama">
    </div>
    @error('age')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Age</label>
        <input type="number" name="age" value="{{$profile->age}}" class="form-control" placeholder="Masukkan umur">
    </div>
    @error('age')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Address</label>
        <textarea name="address" class="form-control">{{$profile->address}}</textarea>
    </div>
     @error('address')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
   <div class="form-group">
        <label>Biodata</label>
        <textarea name="biodata" class="form-control">{{$profile->biodata}}</textarea>
    </div>
     @error('biodata')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Gambar</label>
        <input type="file" name="gambar" class="form-control">
    </div>
    @error('gambar')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Edit</button>
    <a href="/profile" class="btn btn-info">Kembali</a>

</form>
@endsection
@extends('layouts.master')

@section('title')
    Pertanyaan
@endsection

@section('content')

<a href="/pertanyaan/create" class="btn btn-primary btn-sm">Pertanyaan Baru</a><br><br>

<div class="row">
    @forelse($pertanyaan as $item)
    <div class="col-3">
        <div class="card">
            <img src="{{asset('/images/' . $item->gambar)}}" height="200px" class="card-img-top" alt="...">
            <div class="card-body">
              <span class="badge badge-success">{{$item->kategori->kategori}}</span>
              <h5 class="card-title">{{$item->pertanyaan}}</h5>
              <a href="/pertanyaan/{{$item->id}}" class="btn btn-info btn-block">Read More</a>
              <div class="row mt-3">
                <div class="col-6">
                    <a href="/pertanyaan/{{$item->id}}/edit" class="btn btn-warning btn-block">Edit</a>
                </div>
                <div class="col-6">
                    <form action="/pertanyaan/{{$item->id}}" method="post">
                      @csrf
                      @method('delete')
                      <input type="submit" class="btn btn-danger btn=block" value="delete">
                    </form>
                </div>
              </div>
            </div>
          </div>
    </div>
    @empty
        <h1>Tidak ada Pertanyaan</h1>
    @endforelse
</div>

@endsection
@extends('layouts.master')

@section('title')
    Pertanyaan Baru
@endsection

@section('content')

<form action="/pertanyaan" method="post" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label>Pertanyaan</label>
        <input type='text' name="pertanyaan" class="form-control">
    </div>
    @error('pertanyaan')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Kategori</label>
        <select name="kategori_id" class="form-control" id="">
            <option value="">--Pilih Kategori--</option>
            @forelse ($kategori as $item)
                <option value="{{$item->id}}">{{$item->kategori}}</option>
            @empty
                <option value="">Belum ada Kategori</option>
            @endforelse
        </select>
    </div>  
    @error('kategori_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Gambar</label>
        <input type='file' name="gambar" class="form-control">
    </div>  
    @error('gambar')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection
@extends('layouts.master')

@section('title')
    Pertanyaan Baru
@endsection

@section('content')

<form action="/pertanyaan/{{$pertanyaan->id}}" method="post" enctype="multipart/form-data">
    @csrf
    @method('put')
    <div class="form-group">
        <label>Pertanyaan</label>
        <input type='text' name="pertanyaan" value="{{$pertanyaan->pertanyaan}}" class="form-control">
    </div>
    @error('pertanyaan')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Kategori</label>
        <select name="kategori_id" class="form-control" id="">
            <option value="">--Pilih Kategori--</option>
            @forelse ($kategori as $item)
            @if($item->id===$pertanyaan->kategori_id )
                <option value="{{$item->id}}" selected>{{$item->kategori}}</option>
            @else
                <option value="{{$item->id}}">{{$item->kategori}}</option>
            @endif
            @empty
                <option value="">Belum ada Kategori</option>
            @endforelse
        </select>
    </div>  
    @error('kategori_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Gamber</label>
        <input type='file' name="gambar" class="form-control">
    </div>  
    @error('gambar')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@extends('layouts.master')

@section('sub-title')
    Edit Pertanyaan
@endsection

@section('content')

<form action="/pertanyaan/{{$pertanyaan->id}}" method="post" enctype="multipart/form-data">
    @csrf
    @method('put')
    <div class="form-group">
        <label>Pertanyaan</label>
        <input type='text' name="pertanyaan" value="{{$pertanyaan->pertanyaan}}" class="form-control">
    </div>
    @error('pertanyaan')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Kategori</label>
        <select name="kategori_id" class="form-control" id="">
            <option value="">--Pilih Kategori--</option>
            @forelse ($kategori as $item)
                @if ($item->id === $pertanyaan->$kategori_id)
                    <option value="{{$item->id}}" selected>{{$kategori->kategori}}</option>
                @else
                    <option value="{{$item->id}}">{{$kategori->kategori}}"></option>
                @endif
            @empty
                <option value="">Belum ada Kategori</option>
            @endforelse
        </select>
    </div>  
    @error('kategori_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>User</label>
        <select name="user_id" class="form-control" id="">
            <option value="">--Pilih User--</option>
            @forelse ($user as $item)
                @if ($item->id === $pertanyaan->$user_id)
                    <option value="{{$item->id}}" selected>{{$user->name}}</option>
                @else
                    <option value="{{$item->id}}">{{$user->name}}</option>
                @endif
            @empty
                <option value="">Belum ada user</option>
            @endforelse
        </select>
    </div>  
    @error('user_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Gamber</label>
        <input type='file' name="gambar" class="form-control">
    </div>  
    @error('gambar')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection
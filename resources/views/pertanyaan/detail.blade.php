@extends('layouts.master')

@section('sub-title')
    Detail Pertanyaan
@endsection

@section('content')

<div class="card">
    <img src="{{asset('/gambar/' . $pertanyaan->gambar)}}" class="card-img-top" alt="...">
    <div class="card-body">
      <h5 class="card-title">{{$pertanyaan->pertanyaan}}</h5>
    </div>
  </div>

  <a href="/pertanyaan" class="btn btn-secondary btn-sm">Kembali</a> 
  
@endsection
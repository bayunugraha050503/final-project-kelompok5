     <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <div class="text-center sidebar-brand-wrapper d-flex align-items-center">
          <a class="sidebar-brand brand-logo" href="/pertanyaan"><img src="{{asset('/template/template/assets/images/logo.svg')}}" alt="logo" /></a>
          <a class="sidebar-brand brand-logo-mini pl-4 pt-3" href="/"><img src="{{asset('/template/template/assets/images/logo-mini.svg')}}" alt="logo" /></a>
        </div>
        <ul class="nav">
          @auth
          <li class="nav-item nav-profile">
            <a href="/profile" class="nav-link">
              <div class="nav-profile-image">
                <img src="{{asset('/images/'. Auth::user()->profile->gambar )}}" onerror="this.src='{{asset('/images/alt.png')}}'" alt="" />
              </div>
              <div class="nav-profile-text d-flex flex-column pr-3">
                <span class="font-weight-medium mt-1">{{ Auth::user()->name }}</span>
              </div>
            </a>
          </li>
          @endauth
          <li class="nav-item">
            <a class="nav-link" href="/pertanyaan">
              <i class="mdi mdi-home menu-icon"></i>
              <span class="menu-title">Home</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/kategori">
              <i class="mdi mdi-format-list-bulleted menu-icon"></i>
              <span class="menu-title">Kategori</span>
            </a>
          </li>
  
          
        
          
        </ul>
      </nav>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <title>Forum Tanya Jawab</title>
    <link rel="stylesheet" href="{{asset('/template/template/assets/vendors/mdi/css/materialdesignicons.min.css')}}" />
    <link rel="stylesheet" href="{{asset('/template/template/assets/vendors/flag-icon-css/css/flag-icon.min.css')}}" />
    <link rel="stylesheet" href="{{asset('/template/template/assets/vendors/css/vendor.bundle.base.css')}}" />
    <link rel="stylesheet" href="{{asset('/template/template/assets/vendors/font-awesome/css/font-awesome.min.css')}}" />
    <link rel="stylesheet" href="{{asset('/template/template/assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" />
    <link rel="stylesheet" href="{{asset('/template/template/assets/css/style.css')}}" />
    <link rel="shortcut icon" href="{{asset('/template/template/assets/images/favicon.png')}}" />
    @stack('styles')
  </head>
  <body>
    <div class="container-scroller">
    <!-- Include sidebar -->
    @include('layouts.sidebar')
    <!-- End include sidebar -->
      <div class="container-fluid page-body-wrapper">
        <!-- Include setting panel -->
        @include('layouts.setting-panel')
        <!-- End include setting panel -->
       <!-- Include nav -->
       @include('layouts.nav')
       <!-- End include nav -->
        <div class="main-panel">
          <div class="content-wrapper">
            <h1>@yield('title')</h1>
            <div class="row">
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-decription">
                                 @yield('content')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
          <!-- Include footer -->
          @include('layouts.footer')    
          <!-- End include footer -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="{{asset('/template/template/assets/vendors/js/vendor.bundle.base.js')}}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="{{asset('/template/template/assets/vendors/chart.js/Chart.min.js')}}"></script>
    <script src="{{asset('/template/template/assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('/template/template/assets/vendors/flot/jquery.flot.js')}}"></script>
    <script src="{{asset('/template/template/assets/vendors/flot/jquery.flot.resize.js')}}"></script>
    <script src="{{asset('/template/template/assets/vendors/flot/jquery.flot.categories.js')}}"></script>
    <script src="{{asset('/template/template/assets/vendors/flot/jquery.flot.fillbetween.js')}}"></script>
    <script src="{{asset('/template/template/assets/vendors/flot/jquery.flot.stack.js')}}"></script>
    <script src="{{asset('/template/template/assets/vendors/flot/jquery.flot.pie.js')}}"></script>
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{{asset('/template/template/assets/js/off-canvas.js')}}"></script>
    <script src="{{asset('/template/template/assets/js/hoverable-collapse.js')}}"></script>
    <script src="{{asset('/template/template/assets/js/misc.js')}}"></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src="{{asset('/template/template/assets/js/dashboard.js')}}"></script>
    <!-- End custom js for this page -->
    <!-- Stack script -->
    @stack('scripts')
    <!-- End stack script -->
  </body>
</html>
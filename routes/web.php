<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\PertanyaanController;
use App\Http\Controllers\JawabanController;
use App\Http\Controllers\KategoriController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['middleware' => ['auth']], function () {
    //Read & Update Profile
    Route::get('/profile',[ProfileController::class, 'show']);
    Route::get('/profile/edit',[ProfileController::class, 'edit']);
    Route::put('/profile/{id}',[ProfileController::class, 'update']);
    // CRUD kategori
    //route create new game
    Route::get('/kategori/create', [KategoriController::class, 'create']);
    //route for database input
    Route::post('/kategori', [KategoriController::class, 'store']);
    //route untuk tampil data,edit, dan delete
    Route::get('/kategori/{id}/edit', [KategoriController::class, 'edit']);
    Route::put('/kategori/{id}', [KategoriController::class, 'update']);
    Route::delete('/kategori/{id}', [KategoriController::class, 'destroy']);
});

// CRUD kategori
//route view kaetgori
Route::get('/kategori', function () {
    return view('kategori.index');
});
//route create new game
Route::get('/kategori/create', [KategoriController::class, 'create']);
//route for database input
Route::post('/kategori', [KategoriController::class, 'store']);
//route untuk tampil data,edit, dan delete
Route::get('/kategori', [KategoriController::class, 'index']);
Route::get('/kategori/{id}', [KategoriController::class, 'show']);
Route::get('/kategori/{id}/edit', [KategoriController::class, 'edit']);
Route::put('/kategori/{id}', [KategoriController::class, 'update']);
Route::delete('/kategori/{id}', [KategoriController::class, 'destroy']);

Auth::routes();



Route::get('/kategori', [KategoriController::class, 'index']);
Route::get('/kategori/{id}', [KategoriController::class, 'show']);



// CRUD kategori
    //route create new game
    Route::get('/pertanyaan/create', [PertanyaanController::class, 'create']);
    //route for database input
    Route::post('/pertanyaan', [PertanyaanController::class, 'store']);
    //route untuk tampil data,edit, dan delete
    Route::get('/pertanyaan', [PertanyaanController::class, 'index']);
    Route::get('/pertanyaan/{id}', [PertanyaanController::class, 'show']);
    Route::get('/pertanyaan/{id}/edit', [PertanyaanController::class, 'edit']);
    Route::put('/pertanyaan/{id}', [PertanyaanController::class, 'update']);
    Route::delete('/pertanyaan/{id}', [PertanyaanController::class, 'destroy']);


// CRUD pertanyaan
Route::resources('pertanyaan',PertanyaanController::class);
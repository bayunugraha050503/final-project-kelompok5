<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class kategori extends Model
{
    protected $table = 'kategori';
    protected $fillable = ['kategori','deskripsi'];
    use HasFactory;

    public function pertanyaan()
    {
        return $this->hasMany(pertanyaan::class,'kategori_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class pertanyaan extends Model
{
    protected $table = 'pertanyaan';
    protected $fillable = ['pertanyaan','kategori_id','user_id','gambar'];
    use HasFactory;

    public function kategori()
    {
        return $this->belongsTo(kategori::class,'kategori_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}

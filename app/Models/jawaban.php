<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class jawaban extends Model
{
    protected $table = 'jawaban';
    protected $fillable = ['jawaban','pertanyaan_id','user_id','gambar'];
    use HasFactory;
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use File;

class ProfileController extends Controller
{
    public function show()
    {
        $user_id = Auth::id();
        $profile = Profile::where('user_id',$user_id)->first();

        return view('profile.show',['profile'=>$profile]);
    
    }

    public function fotoSidebar()
    {
        $user_id = Auth::id();
        $profile = Profile::where('user_id',$user_id)->first();

        return view('layouts.sidebar',['profile'=>$profile]);
    
    }

    public function edit()
    {
        $user_id = Auth::id();
        $profile = Profile::where('user_id',$user_id)->first();

        return view('profile.edit',['profile'=>$profile]);
    }

    public function update(Request $request, $id)
    {
        $validated = $request->validate([
        'name' => 'required',
        'age' => 'required',
        'address' => 'required',
        'biodata' => 'required',
        'gambar' => 'mimes:png,jpg,jpeg|max:2048',
        ]);

        $profile = Profile::find($id);
        if($request->has('gambar')) {
            $path = 'images/';
            File::delete($path.$profile->gambar);
            $newImage = time().'.'.$request->gambar->extension();
            $request->gambar->move(public_path('images'),$newImage);
            Profile::where('id', $id)
            ->update([
                    'gambar' => $newImage,
                    ]);
        }

        Profile::where('id', $id)
            ->update([
                    'age' => $request['age'],
                    'address' => $request['address'],
                    'biodata' => $request['biodata'],
                    ]);

        $user_id = Auth::id();
        User::where('id', $user_id)
            ->update([
                    'name' => $request['name'],
                    ]);

        return redirect('/profile');
    }
}


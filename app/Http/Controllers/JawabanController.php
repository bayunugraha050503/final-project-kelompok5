<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class JawabanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jawaban = Jawaban::all();
        return view('jawaban.index', ['jawaban'=>$jawaban]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jawaban = jawaban::all();
        return view('jawaban.create', ['jawaban'=> $jawaban]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'jawaban' => 'required',
            'jawaban_id' => 'required',
            'user_id' => 'required',
            'gambar' => 'required|mimes:png,jpg,jpeg|max:2048'
        ]);
    
        //convertion image name
        $gambarName = time().'.'.$request->image->extention();

        //image go to public/gambar
        $request->image->move(public_path('gambar'), $gambarName);

        //insert to database
        $jawaban = New jawaban;

        $jawaban->jawaban = $request->jawaban;
        $jawaban->pertanyaan_id = $request->pertanyaan_id;
        $jawaban->user_id = $request->user_id;
        $jawaban->imgae = $gambarName;

        $jawaban->save();

        return redirect('/jawaban');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $jawaban = jawaban::find($id);

        return view ('jawaban.detail', ['jawaban'=>$jawaban]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pertanyaan = pertanyaan::all();
        $jawaban = jawaban::find($id);

        return view ('jawaban.detail', ['jawaban'=>$jawaban, 'pertanyaan'=>$pertanyaan]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'jawaban' => 'required',
            'pertanyaan_id' => 'required',
            'user_id' => 'required',
            'gambar' => 'mimes:png,jpg,jpeg|max:2048'
        ]);

        $jawaban = jawaban::find($id);

        $jawaban->jawaban = $request->jawaban;
        $jawaban->pertanyaan_id = $request->pertanyaan_id;
        $jawaban->user_id = $request->user_id;

        if ($request ->has('gambar')) {
            $path = 'gambar/';
            File::delete($path . $jawaban->gambar);

            $NewGambarImage = time().'.'.$request->image->extension();

            $request->image->move(public_path('image'), $NewGambarImage);

            $jawaban->gambar = $NewGambarImage;
        }

        $jawaban ->save();

        return redirect('/jawaban');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jawaban = jawaban::find($id);

        $path = 'gambar/';
        File::delete($path. $jawaban->gambar);

        $jawaban->delete();

        return redirect('/jawaban');
    }
}

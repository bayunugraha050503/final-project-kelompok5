<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kategori;
use App\Models\Pertanyaan;
use Illuminate\Support\Facades\Auth;
use File;


class PertanyaanController extends Controller
{
    
    public function index()
    {
        $pertanyaan = Pertanyaan::all();
        return view('pertanyaan.index', ['pertanyaan'=>$pertanyaan]);
    }

    
    public function create()
    {
        $kategori = Kategori::all();
        return view('pertanyaan.create', ['kategori'=> $kategori]);
    }

    
    public function store(Request $request)
    {
        $request->validate([
            'pertanyaan' => 'required',
            'kategori_id' => 'required',
            'gambar' => 'required|mimes:png,jpg,jpeg|max:2048'
        ]);
    
        //convertion image name
        $gambarName = time().'.'.$request->gambar->extension();

        //image go to public/images
        $request->gambar->move(public_path('images'), $gambarName);


        //insert to database
        $pertanyaan = New Pertanyaan;

        $pertanyaan->pertanyaan = $request->pertanyaan;
        $pertanyaan->kategori_id = $request->kategori_id;
        $user_id = Auth::id();
        $pertanyaan->user_id = $user_id;
        $pertanyaan->gambar = $gambarName;

        $pertanyaan->save();

        return redirect('/pertanyaan');
    }

    public function show($id)
    {
        $pertanyaan = Pertanyaan::find($id);

        return view ('pertanyaan.detail', ['pertanyaan'=>$pertanyaan]);
    }

   
    public function edit($id)
    {
        $kategori = Kategori::all();
        $pertanyaan = Pertanyaan::find($id);
        return view('pertanyaan.edit',['pertanyaan' => $pertanyaan, 'kategori'=>$kategori]);
    }

   
    public function update(Request $request, $id)
    {
        $request->validate([
            'pertanyaan' => 'required',
            'kategori_id' => 'required',
            'gambar' => 'mimes:png,jpg,jpeg|max:2048'
        ]);

        $pertanyaan = Pertanyaan::find($id);
        if($request->has('gambar')) {
            $path = 'images/';
            File::delete($path.$pertanyaan->gambar);
            $newImage = time().'.'.$request->gambar->extension();
            $request->gambar->move(public_path('images'),$newImage);
            Pertanyaan::where('id', $id)
            ->update([
                    'gambar' => $newImage,
                    ]);
        }

        Pertanyaan::where('id', $id)
            ->update([
                    'pertanyaan' => $request['pertanyaan'],
                    'kategori_id' => $request['kategori_id'],
                    ]);
        return redirect('/pertanyaan');
    }

   
    public function destroy($id)
    {
        $pertanyaan = Pertanyaan::find($id);

        $path = 'gambar/';
        File::delete($path. $pertanyaan->gambar);

        $pertanyaan->delete();

        return redirect('/pertanyaan');
    }
}

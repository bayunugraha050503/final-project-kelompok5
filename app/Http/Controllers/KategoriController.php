<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class KategoriController extends Controller
{
    public function create() 
    {
        return view('kategori.create');
    }

    public function store(Request $request) 
    {
        //error  validasi
        $request->validate([
            'kategori' => 'required',
            'deskripsi' => 'required'
        ]);

        DB::table('kategori')->insert([
            'kategori' => $request['kategori'],
            'deskripsi' => $request['deskripsi']
        ]);

        return redirect('/kategori');
    }

    public function index() {
        $kategori = DB::table('kategori')->get();

        return view ('kategori.index', ['kategori' => $kategori]);
    }

    public function show($id)
    {
        $kategori = DB::table('kategori')->find($id);
        return view('kategori.detail', ['kategori' => $kategori]);
    }

    public function edit($id)
    {
        $kategori = DB::table('kategori')->where('id', $id)->first();
        return view('kategori.edit', ['kategori' => $kategori]);
    }

    public function update(Request $request, $id)
    {
        //error  validasi
        $request->validate([
            'kategori' => 'required',
            'deskripsi' => 'required'
        ]);
        DB::table('kategori')
        ->where('id', $id)
        ->update(
            [
                'kategori' => $request['kategori'],
                'deskripsi' => $request['deskripsi']
            ]
            );
        return redirect('/kategori');
    }

    public function destroy($id)
    {
        DB::table('kategori')->where('id','=',$id)->delete();
        return redirect('/kategori');
    }
}
